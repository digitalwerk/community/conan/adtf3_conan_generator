# Copyright 2024 Digitalwerk GmbH.
#
#     This Source Code Form is subject to the terms of the Mozilla
#     Public License, v. 2.0. If a copy of the MPL was not distributed
#     with this file, You can obtain one at https://mozilla.org/MPL/2.0/.
#
# If it is not possible or desirable to put the notice in a particular file, then
# You may include the notice in a location (such as a LICENSE file in a
# relevant directory) where a recipient would be likely to look for such a notice.
#
# You may add additional accurate notices of copyright ownership.

from os.path import join
from conans import __version__ as ConanVersion
from conans.model.version import Version
if ConanVersion < Version("1.19"):
    raise Exception("Conan client version 1.19 or higher required to use this Generator")

from conans import ConanFile

import os

class DemoAdtfProjectExample2(ConanFile):
    name = "demo_adtf_project_example2"
    version = "0.0.1"
    description = "Generates startup scripts and adtfenvironment file for demo_adtf_project"
    url = "https://gitlab.com/digitalwerk/community/conan/adtf_conan_generator.git"
    homepage = "https://www.digitalwerk.net/"
    license = "https://gitlab.com/digitalwerk/community/conan/adtf_conan_generator/LICENSE"

    generators = "ADTF3Generator"
    settings = "os", "compiler", "build_type", "arch"

    adtf_requirement = "adtf/3.16.2@dw/stable"
    adtf_conan_generator_requirement = "adtf_conan_generator/1.6.0@dw/stable"
    
    scm = {
        "type": "git",
        "url": "auto",
        "revision": "auto",
    }
    
    script_dir = os.path.dirname(os.path.realpath(__file__))
    adtf3_projects = [
        {
            "VERSION": version,
            "DESCRIPTION": "ADTF Conan Generator Example Project.",
            "ENVIRONMENT_DIRECTORY_MACRO": "EXAMPLE_PROJECT_DIR",
            "BASE_PATH": os.path.join(script_dir, "example", "demo_adtf_project_base"),
            "PROJECT_PATH": "demo_adtf_project_base.adtfproject",
            "SESSION_PATH": "adtfsessions/demo_adtf_session.adtfsession",
            "START_ARGS": {
                    "ce": "", 
                    "launcher": "--console --run",
                    "launcher_with_profiling": "--console --run",
                    "control": "",
                    "guicontrol": ""
                },
            "PLUGIN_DIRECTORIES": {
                "bin",
                "bin/debug"
            },
            "DOCUMENTATION": {
                "Demo ADTF Conan Generator Example Project Documentation": "doc/examples.md"
            },
            "TARGET_FOLDER": "." # not relevant here
        }
    ]

    """
    This variable allows you to generate a new adtf_configuration_editor.cesettings file, enabling you to override the default launchers, tools, and home elements to better meet your needs in the configuration editor.

    The adtf3_ce_settings variable is a dictionary with this main keys:
    1. 'launchers': A list of launcher configurations.
    2. 'tools': A list of all tools in the tools menu.
    3. 'home_elements': A list of entries in the home view
    4. 'remote_settings': A list of all remote connection urls
    """
    adtf3_ce_settings = {
        "launchers": [
            {
                "name": "Launch with enabled profiling",
                "executable": 'cmd.exe /S /K " "$(ADTF_DIR)/bin/adtf_launcher" --environment "$(ADTF_ENVIRONMENT_FILENAME)" --session "$(ADTF_SESSION_FILENAME)" --console --run --profiler "',
                "workingdirectory": '$(ADTF_DIR)/bin',
                "shortcut": 'Shift+F5',
                "icon_path": '$(ADTF_DIR)/settings/LauncherProfiling.svg'
            }
        ],
        "tools": [
            {
                "name": "DDL Editor",
                "executable": '$(ADTF_DIR)/bin/ddl_editor.exe',
                "arguments": "",
                "shortcut": 'Ctrl+Alt+D',
                "icon_path": '$(ADTF_DIR)/settings/ddl_editor.ico'
            },
            {
                "name": "Mapping Editor",
                "executable": '$(ADTF_DIR)/bin/mapping_editor.exe',
                "arguments": "",
                "shortcut": 'Ctrl+Alt+M',
                "icon_path": '$(ADTF_DIR)/settings/mapping_editor.ico'
            },
            {
                "name": "Profiler GUI",
                "executable": '$(ADTF_DIR)/bin/profiler_gui.exe',
                "arguments": "",
                "shortcut": 'Ctrl+Alt+P',
                "icon_path": '$(ADTF_DIR)/settings/profiler_gui.ico'
            }
        ],
        "remote_settings": {
            "enabled": True,
            "fep_discovery": True,
            "remotes": [
                {"url": "http://localhost:8000"}
            ]
        }
    }
    
    def requirements(self):
        self.requires(self.adtf_requirement)
        self.requires(self.adtf_conan_generator_requirement)

    def package(self):
        self.copy("*.*", 
            src=os.path.join(self.source_folder, "example", "projects", "demo_adtf_project_base"), dst="demo_adtf_project_base")
        self.copy("examples.md", 
            src=os.path.join(self.source_folder, "example"), dst="doc")
        self.copy("conanfile_example_2.py",
            src=os.path.join(self.source_folder, "example", "recipes"), dst=os.path.join(self.package_folder, "example"))

    def package_info(self):
        self.adtf3_projects[0]["BASE_PATH"] = os.path.join(self.package_folder, "demo_adtf_project_base")
        self.user_info.ADTF3_PROJECTS = self.adtf3_projects

    def package_id(self):
        self.info.header_only()
